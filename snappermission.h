/**
 * SPDX-FileCopyrightText: 2022 Suhaas Joshi <joshiesuhaas0@gmail.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#pragma once

#include "snapresource.h"

#include <snapd-qt/Snapd/Client>

#include <QString>
#include <QAbstractListModel>

class SnapPermission
{
public:
    SnapPermission();
    SnapPermission(const QString &name, const QString &description, bool isEnabled);

    void setName(const QString &name);
    void setDescription(const QString &description);
    void setEnabled(bool isEnabled);
    void setSlot(QSnapdSlot *slot);

    QString name() const;
    QString description() const;
    bool enabled() const;
    QSnapdSlot *slot() const;

private:
    QString m_name;
    QString m_description;
    QSnapdSlot *m_slot;
    bool m_isEnabled;
};

class SnapPermissionsModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(SnapResource *resource READ resource WRITE setResource NOTIFY resourceChanged)

public:
    SnapPermissionsModel(QObject *parent = nullptr);
    enum Roles {
        Name = Qt::UserRole + 1,
        IsChecked,
    };

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex &index, int role) const override;

    SnapResource *resource();
    void loadPermissions();

Q_SIGNALS:
    void resourceChanged();

public Q_SLOTS:
    void setResource(SnapResource *resource);
    void setPermission(const int index);

private:
    QVector<SnapPermission> m_permissions;
    SnapResource *m_resource;
};
