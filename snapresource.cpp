/**
 * SPDX-FileCopyrightText: 2022 Suhaas Joshi <joshiesuhaas0@gmail.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "snapresource.h"

SnapResource::SnapResource(QObject *parent, const QString &name, const QString &id, const QString &icon)
    : QObject(parent),
      m_name(name),
      m_id(id),
      m_icon(icon)
{
}

QString SnapResource::name() const
{
    return m_name;
}

QString SnapResource::displayName() const
{
    QStringList subStrings = m_name.split(QLatin1Char('-'), Qt::SkipEmptyParts);
    for (int i = 0; i < subStrings.size(); i++) {
        subStrings[i].replace(0, 1, subStrings[i][0].toUpper());
    }
    return subStrings.join(QLatin1Char('-'));
}

QString SnapResource::id() const
{
    return m_id;
}

QString SnapResource::icon() const
{
    return m_icon;
}

SnapResourcesModel::SnapResourcesModel(QObject *parent) : QAbstractListModel(parent)
{
    QSnapdGetAppsRequest *installedApps = m_client.getApps();
    installedApps->runSync();

    for (int i = 0; i < installedApps->appCount(); i++) {
        QSnapdApp *app = installedApps->app(i);
        QString name = app->snap();
        QString id = app->commonId();
        m_resources.insert(i, new SnapResource(this, name, id, name));
    }

    std::sort(m_resources.begin(), m_resources.end(), [] (SnapResource *r1, SnapResource *r2) {
        return r1->name() < r2->name();
    });
}

int SnapResourcesModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }
    return m_resources.count();
}

QHash<int, QByteArray> SnapResourcesModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles.insert(Roles::Name, "name");
    roles.insert(Roles::Icon, "icon");
    roles.insert(Roles::Resource, "resource");
    return roles;
}

QVariant SnapResourcesModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    switch(role) {
    case Roles::Name:
        return m_resources.at(index.row())->displayName();
    case Roles::Icon:
        return m_resources.at(index.row())->icon();
    case Roles::Resource:
        return QVariant::fromValue<SnapResource*>(m_resources.at(index.row()));
    }

    return QVariant();
}
