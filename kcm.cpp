/**
 * SPDX-FileCopyrightText: 2022 Suhaas Joshi <joshiesuhaas0@gmail.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "kcm.h"
#include "snappermission.h"

#include <KPluginFactory>
#include <KLocalizedString>

K_PLUGIN_CLASS_WITH_JSON(KCMSnap, "kcm_snap.json")

class SnapResourcesModel;

KCMSnap::KCMSnap(QObject *parent, const KPluginMetaData &data, const QVariantList &args)
    : KQuickAddons::ManagedConfigModule(parent, data, args),
      m_resourcesModel(new SnapResourcesModel(this))
{
    qmlRegisterUncreatableType<KCMSnap>("org.kde.plasma.kcm.snappermissions", 1, 0, "KCMSnap", QString());
    qmlRegisterType<SnapPermissionsModel>("org.kde.plasma.kcm.snappermissions", 1, 0, "SnapPermissionsModel");
}

#include "kcm.moc"
