/**
 * SPDX-FileCopyrightText: 2022 Suhaas Joshi <joshiesuhaas0@gmail.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#pragma once

#include "snapresource.h"

#include <KQuickAddons/ManagedConfigModule>

class SnapPermissionModel;

class KCMSnap : public KQuickAddons::ManagedConfigModule
{
    Q_OBJECT
    Q_PROPERTY(SnapResourcesModel *resourcesModel MEMBER m_resourcesModel CONSTANT)
public:
    explicit KCMSnap(QObject *parent, const KPluginMetaData &data, const QVariantList &args);

private:
    SnapResourcesModel *m_resourcesModel;
    int m_index;
};
