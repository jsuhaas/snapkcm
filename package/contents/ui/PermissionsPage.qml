import QtQuick 2.0

import org.kde.kcm 1.2 as KCM
import org.kde.kirigami 2.19 as Kirigami
import org.kde.plasma.kcm.snappermissions 1.0

KCM.ScrollViewKCM {
    id: permissionsPage
    title: i18n("Permissions")
    implicitWidth: Kirigami.Units.gridUnit * 15
    framedView: true
    property var res

    Kirigami.PlaceholderMessage {
        text: i18n("Select an application from the list to view its permissions here")
        width: parent.width - (Kirigami.Units.largeSpacing * 4)
        anchors.centerIn: parent
        visible: res === undefined
    }

    view: ListView {
        id: permsView
        visible: res !== undefined
        model: SnapPermissionsModel {
            id: permsModel
            resource: permissionsPage.res
        }
        delegate: Kirigami.CheckableListItem {
            id: permItem
            text: model.name
            checked: model.checked
            onClicked: {
                permsModel.setPermission(model.index)
                /* if there is an error while changing the permission, such as the user
                 * not providing the root password, we don't want the delegate to be checked
                 */
                permItem.checked = model.checked
            }
        }
    }
}
