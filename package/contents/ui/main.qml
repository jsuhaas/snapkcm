/**
 * SPDX-FileCopyrightText: 2022 Suhaas Joshi <joshiesuhaas0@gmail.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

import QtQuick 2.12

import org.kde.kirigami 2.19 as Kirigami
import org.kde.kcm 1.2 as KCM

KCM.ScrollViewKCM {
    id: root
    title: i18n("Snap Applications")
    Kirigami.ColumnView.fillWidth: false
    implicitWidth: Kirigami.Units.gridUnit * 40
    implicitHeight: Kirigami.Units.gridUnit * 20
    framedView: true

    Component.onCompleted: {
        kcm.columnWidth = Kirigami.Units.gridUnit * 15
        kcm.push("PermissionsPage.qml")
    }

    view: ListView {
        model: kcm.resourcesModel
        currentIndex: -1
        delegate: Kirigami.BasicListItem {
            text: model.name
            icon: model.icon
            onClicked: {
                kcm.pop(0)
                kcm.push("PermissionsPage.qml", {res: model.resource})
            }
        }
    }
}
