/**
 * SPDX-FileCopyrightText: 2022 Suhaas Joshi <joshiesuhaas0@gmail.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#pragma once

#include <snapd-qt/Snapd/Client>

#include <QObject>
#include <QAbstractListModel>

class SnapResource : public QObject
{
    Q_OBJECT
public:
    explicit SnapResource(QObject *parent, const QString &name, const QString &id, const QString &icon);

    QString name() const;
    QString displayName() const;
    QString id() const;
    QString icon() const;

private:
    QString m_name;
    QString m_id;
    QString m_icon;
};

class SnapResourcesModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit SnapResourcesModel(QObject *parent = nullptr);

    enum Roles {
        Name = Qt::UserRole + 1,
        Icon,
        Resource
    };

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex &index, int role) const override;

private:
    QVector<SnapResource*> m_resources;
    QSnapdClient m_client;
};
