/**
 * SPDX-FileCopyrightText: 2022 Suhaas Joshi <joshiesuhaas0@gmail.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "snappermission.h"

#include <KLocalizedString>

SnapPermission::SnapPermission() {}

SnapPermission::SnapPermission(const QString &name, const QString &description, bool isEnabled)
    : m_name(name),
      m_description(description),
      m_isEnabled(isEnabled)
{
}

void SnapPermission::setName(const QString &name)
{
    m_name = name;
}

void SnapPermission::setDescription(const QString &description)
{
    m_description = description;
}

void SnapPermission::setEnabled(bool isEnabled)
{
    m_isEnabled = isEnabled;
}

void SnapPermission::setSlot(QSnapdSlot *slot)
{
    m_slot = slot;
}

QString SnapPermission::name() const
{
    return m_name;
}

QString SnapPermission::description() const
{
    return m_description;
}

bool SnapPermission::enabled() const
{
    return m_isEnabled;
}

QSnapdSlot *SnapPermission::slot() const
{
    return m_slot;
}

SnapPermissionsModel::SnapPermissionsModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

int SnapPermissionsModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }
    return m_permissions.count();
}

QHash<int, QByteArray> SnapPermissionsModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles.insert(Roles::Name, "name");
    roles.insert(Roles::IsChecked, "checked");
    return roles;
}

QVariant SnapPermissionsModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    switch(role) {
    case Roles::Name:
        return m_permissions.at(index.row()).name();
    case Roles::IsChecked:
        return m_permissions.at(index.row()).enabled();
    }

    return QVariant();
}

SnapResource *SnapPermissionsModel::resource()
{
    return m_resource;
}

void SnapPermissionsModel::setResource(SnapResource *resource)
{
    if (m_resource != resource) {
        beginResetModel();
        m_resource = resource;
        loadPermissions();
        endResetModel();
        Q_EMIT resourceChanged();
    }
}

void SnapPermissionsModel::setPermission(const int index)
{
    QSnapdClient client;
    SnapPermission *perm = &m_permissions[index];

    QSnapdRequest *req;
    bool isEnabled = perm->enabled();
    if (isEnabled) {
        req = client.disconnectInterface(m_resource->name(), perm->name(), perm->slot()->snap(), perm->slot()->name());
    } else {
        req = client.connectInterface(m_resource->name(), perm->name(), perm->slot()->snap(), perm->slot()->name());
    }
    req->runSync();
    if (req->error()) {
        qWarning() << "snapd error" << req->errorString();
    } else {
        perm->setEnabled(!isEnabled);
    }
}

void SnapPermissionsModel::loadPermissions()
{
    QSnapdClient client;
    auto req = client.getInterfaces();
    req->runSync();

    QHash<QString, QVector<QSnapdSlot*>> slotsForInterface;
    for (int i = 0; i < req->slotCount(); i++) {
        const auto slot = req->slot(i);
        slot->setParent(this);
        slotsForInterface[slot->interface()].append(slot);
    }

    const QString appName = m_resource->name();
    for (int i = 0; i < req->plugCount(); i++) {
        const QScopedPointer<QSnapdPlug> plug(req->plug(i));
        if (plug->snap() == appName) {
            if (plug->interface() == QLatin1String("content")) {
                continue;
            }
            const auto theSlots = slotsForInterface.value(plug->interface());
            for (int j = 0; j < theSlots.count(); j++) {
                SnapPermission perm;
                perm.setName(plug->label().isEmpty() ? plug->name() : i18n("%1 - %2", plug->name(), plug->label()));
                perm.setEnabled(plug->connectedSlotCount() > 0);
                perm.setSlot(theSlots.at(j));
                m_permissions.append(perm);
            }
        }
    }
}
